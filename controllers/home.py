from flask import Flask, Blueprint, render_template, abort, jsonify
from jinja2 import TemplateNotFound
from utils import get_influx_db

home_page = Blueprint('home_page', __name__, template_folder='templates')

@home_page.route('/')
def show():
    return render_template('home.html')

@home_page.route('/get_monitor_data', methods=['GET'])
def get_monitor_data():
    # Create a json object with the requested parameters
    # Retrieve first the hv data
    db = get_influx_db()
    results = db.query('''
        select "V0Set" as V0Set_RPC0, "IMon" as IMon_RPC0, "Status" as Status_RPC0 
        from hv 
        where (module = 'hvrpc256new' 
        and channel_name = 'RPC0')
        order by desc 
        limit 1;
        select "V0Set" as V0Set_RPC1    , "IMon" as IMon_RPC1   , "Status" as Status_RPC1   
        from hv 
        where (module = 'hvrpc256new' 
        and channel_name = 'RPC1')
        order by desc 
        limit 1;
        select "Float_value" as upstream_attenuation
        from dip
        where sub = 'dip/GIFpp/Attenuators/UpStreamPos/EffectiveAttenuation'
        order by desc
        limit 1;
        select "Float_value" as downstream_attenuation
        from dip
        where sub = 'dip/GIFpp/Attenuators/DownStreamPos/EffectiveAttenuation'
        order by desc
        limit 1;
        select "Int_value" as source_on
        from dip
        where sub = 'dip/GIFpp/Irradiator/SourceON'
        order by desc
        limit 1;
        select "Float_value" as temperature_bunker
        from dip
        where sub = 'dip/GIFpp/Temp_Inside_Bunker'
        order by desc
        limit 1;
        select "Float_value" as humidity_bunker
        from dip
        where sub = 'dip/GIFpp/Humidity_Inside_Bunker'
        order by desc
        limit 1;
        select "Float_value" as pressure_bunker
        from dip
        where sub = 'dip/GIFpp/Atmospheric_Pressure'
        order by desc
        limit 1;
        
        select "mfc0" as CO2, "mfc1" as R134a, "mfc2" as HFO, "mfc3" as SF6, "mfc4" as iC4H10
        from "flowplot_percent"
        WHERE ("place" = 'gif' AND "setup" = 'rpc')
        order by desc
        limit 1;
        select "mfc0" as CO2, "mfc1" as R134a, "mfc2" as HFO, "mfc3" as SF6, "mfc4" as iC4H10
        from "flowplot_flows"
        WHERE ("place" = 'gif' AND "setup" = 'rpc')
        order by desc
        limit 1;
        SELECT "mfc0" + "mfc1" + "mfc2" + "mfc3" + "mfc4" 
        FROM "flowplot_flows" 
        WHERE ("place" = 'gif' AND "setup" = 'rpc')
        ORDER by desc
        LIMIT 1;
        SELECT "H2O" 
        FROM "picolog" WHERE ("place" = 'gif')
        ORDER by desc
        LIMIT 1;
    ''')
    return jsonify([result.raw for result in results])
