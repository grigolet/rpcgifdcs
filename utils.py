from influxdb import InfluxDBClient
import os

def get_influx_db():
    """Return an instance to the influxdb db client"""
    host = os.getenv('INFLUX_HOST')
    port = int(os.getenv('INFLUX_PORT'))
    user = os.getenv('INFLUX_USER')
    password = os.getenv('INFLUX_PASSWORD')
    database = os.getenv('INFLUX_DATABASE')
    ssl = os.getenv('INFLUX_SSL') == 'true'
    verify_ssl = os.getenv('INFLUX_VERIFYSSL') == 'true'
    client = InfluxDBClient(host, port, user, password,
                            database, ssl=ssl, verify_ssl=verify_ssl)
    return client
