# RPC DCS at GIF++

The website used flask to run a web server. The functionality of the web server are:

* monitor: current gif and detectors parameters
* control: change detectors chambers and status
* alert: control values and send alarms
* acquisition: acquire data from detectors
* storage: use and external database to save run data


TODO:

- [] monitor
- [] alert
- [] configuration
- [] control
- [] storage

