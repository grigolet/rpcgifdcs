from flask import Flask
from controllers.home import home_page
from dotenv import load_dotenv
import os
configs = load_dotenv()

app = Flask(__name__)
app.register_blueprint(home_page)
app.run(debug=True, host='0.0.0.0')

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'influxdb'):
        g.influxdb.close()